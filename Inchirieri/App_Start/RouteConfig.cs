﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Inchirieri
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "CarEdit",
                url: "{controller}/{action}/{carId}",
                defaults: new { controller = "Cars", action = "Edit", carId = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Cars",
                url: "{controller}/{action}/{carId}",
                defaults: new { controller = "Cars", action = "Index", carId = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "SortByType",
            //    url: "cars/sort/{year}/{fuel}",
            //    defaults: new {controller = "Cars", action = "SortByType"}
            //    );
        }
    }
}
