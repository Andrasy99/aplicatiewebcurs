﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Inchirieri.Models;

namespace Inchirieri.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Users = GetUsers();
            ViewBag.Orders = GetOrders();

            return View();
        }

        [Route("admin/order/{orderId}")]
        public ActionResult ReturnOrder(int orderId)
        {
            SqlConnection conn = DataBaseConnect();
            string sqlCmd = $"UPDATE Rentals SET order_returnedAt = SYSDATETIME(), updatedAt = SYSDATETIME() WHERE order_id = {orderId};";
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.ExecuteNonQuery();
            conn.Close();

            return RedirectToAction("Index");
        }

        [HttpGet]
        [Route("admin/edit/{userId}")]
        public ActionResult Edit(string userId)
        {
            ViewBag.Roles = GetRoles();

            User user = new User();
            List <User> users = GetUsers();
            user = users.Find(x => x.Id == userId);

            return View(user);
        }

        [HttpPost]
        [Route("admin/edit/{userId}")]
        public ActionResult Edit(string userId, string userRole)
        {
            string sqlCmd = "UPDATE AspNetUserRoles SET RoleId=@role WHERE UserId=@userId;";
            SqlConnection conn = DataBaseConnect();
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.Parameters.AddWithValue("@role", userRole);
            cmd.Parameters.AddWithValue("@userId", userId);
            cmd.ExecuteNonQuery();

            conn.Close();

            return RedirectToAction("Index");
        }

        // SAMPLE DATA LISTS //

        public List<User> GetUsers()
        {
            SqlConnection conn = DataBaseConnect();
            string cmd = "SELECT AspNetUsers.Id, AspNetUsers.Email, AspNetUsers.UserName, AspNetRoles.Name FROM AspNetUsers, AspNetUserRoles, AspNetRoles WHERE AspNetUsers.Id = AspNetUserRoles.UserId AND AspNetUserRoles.RoleId = AspNetRoles.Id;";
            SqlDataReader reader = ExecuteQuery(sqlCmd: cmd, conn: conn);

            List<User> users = new List<User>();

            while (reader.Read())
            {
                User user = new User
                {
                    Id = reader["Id"].ToString(),
                    Email = reader["Email"].ToString(),
                    Name = reader["UserName"].ToString(),
                    Role = reader["Name"].ToString()
                };

                users.Add(user);
            }
            conn.Close();

            return users;
        }

        public Dictionary<int,string> GetRoles()
        {
            Dictionary<int, string> roles = new Dictionary<int, string>();

            SqlConnection conn = DataBaseConnect();
            string cmd = "SELECT * FROM AspNetRoles;";
            SqlDataReader reader = ExecuteQuery(cmd, conn);
            while (reader.Read())
            {
                roles.Add(int.Parse(reader["Id"].ToString()), reader["Name"].ToString());
            }
            conn.Close();

            return roles;
        }

        public List<Order> GetOrders()
        {
            List<Order> orders = new List<Order>();
            SqlConnection conn = DataBaseConnect();
            string sqlCmd = "SELECT Rentals.order_id, Rentals.order_carId, Rentals.order_startedAt, Rentals.order_endedAt, Rentals.order_totalPrice, Rentals.createdAt, Rentals.order_returnedAt, AspNetUsers.Email FROM Rentals, AspNetUsers WHERE Rentals.order_custId = AspNetUsers.Id;";
            SqlDataReader reader = ExecuteQuery(sqlCmd, conn);

            while (reader.Read())
            {
                Order order = new Order();
                order.OrderId = int.Parse(reader["order_id"].ToString());
                order.CarId = int.Parse(reader["order_carId"].ToString());
                order.UserId = reader["Email"].ToString();
                order.StartDate = reader["order_startedAt"].ToString();
                order.EndDate = reader["order_endedAt"].ToString();
                order.TotalPrice = int.Parse(reader["order_totalPrice"].ToString());
                order.CreatedAt = reader["createdAt"].ToString();

                if(DBNull.Value.Equals(reader["order_returnedAt"]))
                {
                    order.IsReturned = false;
                    order.ReturnDate = "-";
                }
                else
                {
                    order.IsReturned = true;
                    order.ReturnDate = reader["order_returnedAt"].ToString();
                }

                orders.Add(order);
            }
            conn.Close();

            return orders;
        }

        // SQL HELPER METHODS //

        private SqlConnection DataBaseConnect()
        {
            string connectionString = @"Data Source= ADMINRG-SUVERNL\SQLEXPRESS; Initial Catalog= InchirieriWebApp; Integrated Security= True";
            SqlConnection connection;
            connection = new SqlConnection(connectionString);
            connection.Open();
            return connection;
        }

        private SqlDataReader ExecuteQuery(string sqlCmd, SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            return reader;
        }
    }
}