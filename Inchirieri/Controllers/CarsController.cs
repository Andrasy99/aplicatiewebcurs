﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Inchirieri.Models;
using Microsoft.AspNet.Identity;

namespace Inchirieri.Controllers
{
    public class CarsController : Controller
    {
        [Route("cars")]
        public ActionResult Index()
        {
            List<Car> cars = SampleDataCars();
            ViewBag.Cars = cars;
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int? carId)
        {
            Car masina = new Car();
            List<Car> cars = SampleDataCars();
            Dictionary<int, string> fuel = GetFuelType();
            Dictionary<int, string> trs = GetTransmissions();
            Dictionary<int, string> colors = GetColor();

            if (carId == null)
            {
                masina = cars.First();
            }
            else
            {
                masina = cars.Find(x => x.Id == carId);
            }

            ViewBag.Id = carId;
            ViewBag.FuelTypes = fuel;
            ViewBag.Transmissions = trs;
            ViewBag.Colors = colors;

            return View(masina);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int carId, string carModel, int carRegistration, int fuelType, int carSeats, string color, int transmission, int carPrice)
        {
            string sqlCmd = "UPDATE Cars SET car_model = @model, car_fabricatedAt = @carRegistration, car_fuel = @fuelType, car_seats = @carSeats, car_color = @color, car_transmission = @trs, car_rent_price = @carPrice, updatedAt = SYSDATETIME() WHERE car_id = @carId;";

            SqlConnection conn = DataBaseConnect();
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.Parameters.AddWithValue("@model", carModel);
            cmd.Parameters.AddWithValue("@carRegistration", carRegistration);
            cmd.Parameters.AddWithValue("@fuelType", fuelType);
            cmd.Parameters.AddWithValue("@carSeats", carSeats);
            cmd.Parameters.AddWithValue("@color", color);
            cmd.Parameters.AddWithValue("@trs", transmission);
            cmd.Parameters.AddWithValue("@carPrice", carPrice);
            cmd.Parameters.AddWithValue("@carId", carId);
            cmd.ExecuteNonQuery();
            conn.Close();

            return RedirectToAction("Index");
        }

        [Route("cars/delete/{carId}")]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int carId)
        {
            SqlConnection conn = DataBaseConnect();

            string sqlCmd = "DELETE From Cars WHERE car_id=" + carId;
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.ExecuteNonQuery();
            conn.Close();

            return RedirectToAction("/");
        }

        [HttpGet]
        [Route("cars/add")]
        public ActionResult Add()
        {
            Car masina = new Car();
            Dictionary<int, string> fuel = GetFuelType();
            Dictionary<int, string> trs = GetTransmissions();
            Dictionary<int, string> colors = GetColor();

            ViewBag.FuelType = fuel;
            ViewBag.Transmission = trs;
            ViewBag.Color = colors;

            return View(masina);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("cars/add")]
        public ActionResult Add(string carModel, int carRegistration, int fuelType, int carSeats, string color, int transmission, int carPrice)
        {
            string sqlCmd = "INSERT INTO Cars (car_model, car_fuel, car_seats, car_color, car_transmission, car_rent_price, car_fabricatedAt, createdAt)" +
                            "VALUES (@model, @fuelType, @carSeats, @color, @trs, @carPrice, @carRegistration, SYSDATETIME());";

            SqlConnection conn = DataBaseConnect();
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.Parameters.AddWithValue("@model", carModel);
            cmd.Parameters.AddWithValue("@carRegistration", carRegistration);
            cmd.Parameters.AddWithValue("@fuelType", fuelType);
            cmd.Parameters.AddWithValue("@carSeats", carSeats);
            cmd.Parameters.AddWithValue("@color", color);
            cmd.Parameters.AddWithValue("@trs", transmission);
            cmd.Parameters.AddWithValue("@carPrice", carPrice);
            cmd.ExecuteNonQuery();
            conn.Close();

            return RedirectToAction("Index");
        }

        [Route("cars/view/{carId}")]
        public ActionResult View(int carId)
        {
            Car car = new Car();
            List<Car> cars = SampleDataCars();

            if(carId == -1)
            {
                car = cars.First();
            }
            else
            {
                car = cars.Find(x => x.Id == carId);
            }
            ViewBag.Image = "/Content/Images/car-" + carId + ".jpg";

            return View(car);
        }

        [HttpGet]
        [Authorize(Roles = "User")]
        [Route("cars/rent/{carId}")]
        public ActionResult Rent(int? carId)
        {
            Car masina = new Car();
            List<Car> cars = SampleDataCars();

            if (carId == null)
            {
                masina = cars.First();
            }
            else
            {
                masina = cars.Find(x => x.Id == carId);
            }

            return View(masina);
        }

        [HttpPost]
        [Authorize(Roles = "User")]
        [Route("cars/rent/{carId}")]
        public ActionResult Rent(int carId,string startDate, string endDate, int totalPrice)
        {
            var userId = User.Identity.GetUserId();
            SqlConnection conn = DataBaseConnect();
            //string sqlCmd = $"INSERT INTO Rentals(order_carId, order_custId, order_startedAt, order_endedAt, order_totalPrice, createdAt) VALUES ({carId}, {userId}, {startDate}, {endDate}, {totalPrice}, SYSDATETIME());";

            string sqlCmd = "INSERT INTO Rentals(order_carId, order_custId, order_startedAt, order_endedAt, order_totalPrice, createdAt) VALUES (@carId, @custId, @started, @ended, @price, SYSDATETIME());";
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.Parameters.AddWithValue("@carId", carId);
            cmd.Parameters.AddWithValue("@custId", userId);
            cmd.Parameters.AddWithValue("@started", startDate);
            cmd.Parameters.AddWithValue("@ended", endDate);
            cmd.Parameters.AddWithValue("@price", totalPrice);

            cmd.ExecuteNonQuery();
            conn.Close();

            return RedirectToAction("Rent");
        }

        // SQL DATABASE LISTS //

        public Dictionary<int, string> GetTransmissions()
        {
            SqlConnection conn = DataBaseConnect();

            string sqlCmd = "SELECT * FROM Transmissions";
            SqlDataReader reader = ExecuteQuery(sqlCmd, conn);

            Dictionary<int, string> trs = new Dictionary<int, string>();

            while (reader.Read())
            {
                int trId = int.Parse(reader["tr_id"].ToString());
                string trVal = reader["tr_name"].ToString();

                trs[trId] = trVal;
            }
            conn.Close();

            return trs;
        }

        public Dictionary<int, string> GetFuelType()
        {
            SqlConnection conn = DataBaseConnect();

            string sqlCmd = "SELECT * FROM FuelTypes";

            SqlDataReader reader = ExecuteQuery(sqlCmd, conn);

            Dictionary<int, string> fuel = new Dictionary<int, string>();

            while (reader.Read())
            {
                int fuelId = int.Parse(reader["fuel_id"].ToString());
                string fuelVal = reader["fuel_name"].ToString();

                fuel[fuelId] = fuelVal;
            }
            conn.Close();

            return fuel;
        }

        public Dictionary<int, string> GetColor()
        {
            SqlConnection conn = DataBaseConnect();

            string sqlCmd = "SELECT * FROM Colors";
            SqlDataReader reader = ExecuteQuery(sqlCmd, conn);

            Dictionary<int, string> color = new Dictionary<int, string>();

            while (reader.Read())
            {
                int colorId = int.Parse(reader["color_id"].ToString());
                string colorVal = reader["color_name"].ToString();

                color[colorId] = colorVal;
            }
            conn.Close();

            return color;
        }

        public List<Car> SampleDataCars()
        {
            List<Car> cars = new List<Car>();
            SqlConnection conn = DataBaseConnect();

            string sqlCmd = "SELECT Cars.*, FuelTypes.fuel_name, Transmissions.tr_name FROM Cars, FuelTypes, Transmissions WHERE Cars.car_fuel = FuelTypes.fuel_id AND Cars.car_transmission = Transmissions.tr_id";
            SqlDataReader reader = ExecuteQuery(sqlCmd, conn);

            while (reader.Read())
            {
                Car masina = new Car();

                masina.Id = int.Parse(reader["car_id"].ToString());
                masina.Model = reader["car_model"].ToString();
                masina.RegDate = int.Parse(reader["car_FabricatedAt"].ToString());
                masina.Fuel = reader["fuel_name"].ToString();
                masina.Seats = int.Parse(reader["car_seats"].ToString());
                masina.Color = reader["car_color"].ToString();
                masina.Gearbox = reader["tr_name"].ToString();
                masina.Price = int.Parse(reader["car_rent_price"].ToString());

                cars.Add(masina);
            }
            conn.Close();

            return cars;
        }

        // SQL HELPER METHODS //

        private SqlConnection DataBaseConnect()
        {
            string connectionString = @"Data Source= ADMINRG-SUVERNL\SQLEXPRESS; Initial Catalog= InchirieriWebApp; Integrated Security= True";
            SqlConnection connection;
            connection = new SqlConnection(connectionString);
            connection.Open();
            return connection;
        }

        private SqlDataReader ExecuteQuery(string sqlCmd, SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            return reader;
        }
    }
}