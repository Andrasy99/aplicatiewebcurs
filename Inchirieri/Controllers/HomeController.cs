﻿using Inchirieri.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Inchirieri.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<Car> cars = SampleDataCars();
            ViewBag.Cars = cars;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Route("orders")]
        [Authorize(Roles = "User")]
        public ActionResult Orders()
        {
            List<Order> orders = new List<Order>();
            SqlConnection conn = DataBaseConnect();
            string sqlCmd = "SELECT Rentals.order_id, Rentals.order_carId, Rentals.order_startedAt, Rentals.order_endedAt, Rentals.order_totalPrice, Rentals.createdAt, Rentals.order_returnedAt, AspNetUsers.Email FROM Rentals, AspNetUsers WHERE Rentals.order_custId = AspNetUsers.Id AND Rentals.order_custId = '" + User.Identity.GetUserId() + "';";
            SqlDataReader reader = ExecuteQuery(sqlCmd, conn);

            while (reader.Read())
            {
                Order order = new Order();
                order.OrderId = int.Parse(reader["order_id"].ToString());
                order.CarId = int.Parse(reader["order_carId"].ToString());
                order.UserId = reader["Email"].ToString();
                order.StartDate = reader["order_startedAt"].ToString();
                order.EndDate = reader["order_endedAt"].ToString();
                order.TotalPrice = int.Parse(reader["order_totalPrice"].ToString());
                order.CreatedAt = reader["createdAt"].ToString();

                if(DBNull.Value.Equals(reader["order_returnedAt"]))
                {
                    order.IsReturned = false;
                    order.ReturnDate = "-";
                }
                else
                {
                    order.IsReturned = true;
                    order.ReturnDate = reader["order_returnedAt"].ToString();
                }
                orders.Add(order);
            }
            ViewBag.Orders = orders;
            conn.Close();

            return View();
        }

        [Route("orders/cancel/{orderId}")]
        public ActionResult CancelOrder(int orderId)
        {
            SqlConnection conn = DataBaseConnect();
            string sqlCmd = $"UPDATE Rentals SET order_returnedAt = SYSDATETIME() WHERE order_id = " + orderId;
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            cmd.ExecuteNonQuery();

            return RedirectToAction("Orders");
        }

        // SQL HELPER METHODS //

        private SqlConnection DataBaseConnect()
        {
            string connectionString = @"Data Source= ADMINRG-SUVERNL\SQLEXPRESS; Initial Catalog= InchirieriWebApp; Integrated Security= True";
            SqlConnection connection;
            connection = new SqlConnection(connectionString);
            connection.Open();
            return connection;
        }

        private SqlDataReader ExecuteQuery(string sqlCmd, SqlConnection conn)
        {
            SqlCommand cmd = new SqlCommand(sqlCmd, conn);
            SqlDataReader reader = cmd.ExecuteReader();

            return reader;
        }

        // SAMPLE DATA METHODS //

        public List<Car> SampleDataCars()
        {
            List<Car> cars = new List<Car>();
            SqlConnection conn = DataBaseConnect();

            string sqlCmd = "SELECT Cars.*, FuelTypes.fuel_name, Transmissions.tr_name FROM Cars, FuelTypes, Transmissions WHERE Cars.car_fuel = FuelTypes.fuel_id AND Cars.car_transmission = Transmissions.tr_id";
            SqlDataReader reader = ExecuteQuery(sqlCmd, conn);

            while (reader.Read())
            {
                Car masina = new Car();

                masina.Id = int.Parse(reader["car_id"].ToString());
                masina.Model = reader["car_model"].ToString();
                masina.RegDate = int.Parse(reader["car_FabricatedAt"].ToString());
                masina.Fuel = reader["fuel_name"].ToString();
                masina.Seats = int.Parse(reader["car_seats"].ToString());
                masina.Color = reader["car_color"].ToString();
                masina.Gearbox = reader["tr_name"].ToString();
                masina.Price = int.Parse(reader["car_rent_price"].ToString());

                cars.Add(masina);
            }
            conn.Close();

            return cars;
        }
    }
}